﻿namespace TestProjectTopSystems;

public interface IDrawable
{
    void Draw();
}
