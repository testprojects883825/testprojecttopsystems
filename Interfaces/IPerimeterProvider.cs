﻿namespace TestProjectTopSystems;

public interface IPerimeterProvider
{
    int GetPerimeter();
}
