﻿namespace TestProjectTopSystems;

public interface IAreaProvider
{
    int GetArea();
}
