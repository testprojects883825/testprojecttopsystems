﻿namespace TestProjectTopSystems;

public class Rectangle : Polygon, IAreaProvider
{
    private int side1;
    private int side2;

    public Rectangle(Point point1, Point point2) 
        : base(point1, new Point(point2.X, point1.Y), point2, new Point(point1.X, point2.Y))
    {
        
        side1 = (Points[1] - Points[0]).GetMagnitude();
        side2 = (Points[3] - Points[0]).GetMagnitude();
    }

    public int GetArea()
    {
        return side1 * side2;
    }
}
