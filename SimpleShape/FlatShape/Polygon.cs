﻿namespace TestProjectTopSystems;

public class Polygon : FlatShapeAbstarct, IPerimeterProvider
{
    public Polygon(params Point[] points)
    {
        if (points.Length < 3)
        {
            throw new ArgumentException("The number of vertices must be at least 3!");
        }

        Points = points;
    }

    public Point[] Points { get; protected set; }

    public override void Draw()
    {
        for (int k = 1; k < Points.Length; ++k)
        {
            new Line(Points[k - 1], Points[k]) { Color = Color }.Draw();
        }

        new Line(Points[Points.Length - 1], Points[0]) { Color = Color }.Draw();
    }

    public int GetPerimeter()
    {
        var perimeter = 0;

        for (int k = 1; k < Points.Length; ++k)
        {
            perimeter += (Points[k] - Points[k - 1]).GetMagnitude();
        }

        return perimeter + (Points[0] - Points[Points.Length - 1]).GetMagnitude();
    }
}
