﻿namespace TestProjectTopSystems;

public class Circle : FlatShapeAbstarct, IPerimeterProvider, IAreaProvider
{
    public Circle(Point center, int radius)
    {
        Center = center;
        Radius = radius;
    }

    public Point Center { get; }

    public int Radius { get; }

    public override void Draw()
    {
        throw new NotImplementedException();
    }

    public int GetArea()
    {
        throw new NotImplementedException();
    }

    public int GetPerimeter()
    {
        throw new NotImplementedException();
    }
}
