﻿namespace TestProjectTopSystems;

public class Triangle : Polygon
{
    public Triangle(Point point1, Point point2, Point point3) 
        : base(point1, point2, point3) { }
}
