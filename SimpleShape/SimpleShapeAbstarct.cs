﻿namespace TestProjectTopSystems;

public abstract class SimpleShapeAbstarct : IDrawable
{
    public ConsoleColor Color { get; init; } = ConsoleColor.White;
    public abstract void Draw();
}
