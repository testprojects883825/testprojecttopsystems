﻿namespace TestProjectTopSystems;

public class Point : SimpleShapeAbstarct
{
    public Point(int x, int y)
    {
        X = x;
        Y = y;
    }

    public int X { get; }

    public int Y { get; }

    public int GetMagnitude()
    {
        return (int)Math.Sqrt(X * X + Y * Y);
    }

    public override void Draw()
    {
        throw new NotImplementedException();
    }

    public static Point operator +(Point a, Point b)
    {
        return new Point(a.X + b.X, a.Y + b.Y);
    }

    public static Point operator -(Point a, Point b)
    {
        return new Point(a.X - b.X, a.Y - b.Y);
    }
}
