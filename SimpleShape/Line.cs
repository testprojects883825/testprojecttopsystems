﻿namespace TestProjectTopSystems;

public class Line : SimpleShapeAbstarct
{
    public Line(Point point1, Point point2)
    {
        Point1 = point1;
        Point2 = point2;
    }

    public Point Point1 { get; }

    public Point Point2 { get; }

    public override void Draw()
    {
        // this code is taken from:
        // https://stackoverflow.com/questions/11678693/all-cases-covered-bresenhams-line-algorithm
        var x = Point1.X;
        var y = Point1.Y;
        int w = Point2.X - Point1.X;
        int h = Point2.Y - Point1.Y;
        int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
        if (w < 0) dx1 = -1; else if (w > 0) dx1 = 1;
        if (h < 0) dy1 = -1; else if (h > 0) dy1 = 1;
        if (w < 0) dx2 = -1; else if (w > 0) dx2 = 1;
        int longest = Math.Abs(w);
        int shortest = Math.Abs(h);
        if (!(longest > shortest))
        {
            longest = Math.Abs(h);
            shortest = Math.Abs(w);
            if (h < 0) dy2 = -1; else if (h > 0) dy2 = 1;
            dx2 = 0;
        }
        int numerator = longest >> 1;
        for (int i = 0; i <= longest; i++)
        {
            Console.SetCursorPosition(x, y);
            Console.ForegroundColor = Color;
            Console.Write('0');
            numerator += shortest;
            if (!(numerator < longest))
            {
                numerator -= longest;
                x += dx1;
                y += dy1;
            }
            else
            {
                x += dx2;
                y += dy2;
            }
        }

        Console.ResetColor();
    }
}
