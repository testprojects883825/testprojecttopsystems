﻿namespace TestProjectTopSystems;

internal class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine($"Прямоугольник");
        SimpleShapeAbstarct rectangle = new Rectangle(new Point(3, 3), new Point(41, 21));
        Console.WriteLine(ShapeMetrics.GetMetrics(rectangle));
        (rectangle as IDrawable)?.Draw();
        Console.ReadLine();
        Console.Clear();

        Console.WriteLine($"Треугольник");
        SimpleShapeAbstarct triangle = new Triangle(new Point(4, 4), new Point(88, 13), new Point(32, 23));
        Console.WriteLine(ShapeMetrics.GetMetrics(triangle));
        (triangle as IDrawable)?.Draw();
        Console.ReadLine();
        Console.Clear();

        Console.WriteLine($"Отрезок");
        SimpleShapeAbstarct line = new Line(new Point(88, 4), new Point(4, 23));
        Console.WriteLine(ShapeMetrics.GetMetrics(line));
        (line as IDrawable)?.Draw();
        Console.ReadLine();
        Console.Clear();
    }
}
