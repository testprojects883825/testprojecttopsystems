﻿using System.Text;

namespace TestProjectTopSystems;

public static class ShapeMetrics
{
    public static string GetMetrics(SimpleShapeAbstarct shape)
    {
        var str = new StringBuilder();

        str.Append($"Color:{shape.Color} ");

        if (shape is IAreaProvider)
        {
            str.Append($"Area:{((IAreaProvider)shape).GetArea()} ");
        }

        if (shape is IPerimeterProvider)
        {
            str.Append($"Perimeter:{((IPerimeterProvider)shape).GetPerimeter()} ");
        }

        return str.ToString();
    }
}
