# Тестовый проект для ЗАО "Топ Системы"

 ### Проект реализован на основании тз:

Необходимо привести пример программы или библиотеки на С#, которая выводит на экран различные геометрические фигуры: круг, эллипс, треугольник, квадрат, четырёхугольник и любые другие по желанию.
Глубина проработки примера (параметры, методы) на Ваше усмотрение. Программа не обязательно должна запускаться и работать (хотя это будет плюсом). В задании Вам необходимо продемонстрировать умение использовать ООП.

### Помимо ООП в проекте соблюдаются принципы SOLID, так как зачастую соблюдение этих принципов неразрывно связывают с ООП в качестве лучших практик его применения.

### Программа запускается в консоли и полностью работоспособна, но в случае внесения изменения в код необходимо иметь ввиду, что не все методы имеют реализацию и представлены в демонстрационных целях.